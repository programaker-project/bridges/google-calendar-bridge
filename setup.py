from setuptools import setup

setup(
    name="programaker-calendar-service",
    version="0.1",
    description="Programaker service to interact with a Google Calendar.",
    author="kenkeiras",
    author_email="kenkeiras@codigoparallevar.com",
    license="Apache License 2.0",
    packages=["programaker_gcalendar_service"],
    scripts=["bin/programaker-gcalendar-service"],
    include_package_data=True,
    install_requires=[
        "google-auth-oauthlib",
        "google-api-python-client",
        "programaker-bridge",
        "sqlalchemy",
        "xdg",
    ],
    zip_safe=False,
)
