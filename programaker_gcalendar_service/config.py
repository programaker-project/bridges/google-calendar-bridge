import getpass
import json
import os

from xdg import XDG_CONFIG_HOME

BRIDGE_ENDPOINT_INDEX = "bridge_endpoint"
AUTH_TOKEN_INDEX = "authentication_token"

CLIENT_CREDENTIALS_PATH_ENV = "CLIENT_CREDENTIALS_PATH"
BRIDGE_ENDPOINT_ENV = "BRIDGE_ENDPOINT"
AUTH_TOKEN_ENV = "BRIDGE_AUTH_TOKEN"

DIRECTORY = os.path.join(XDG_CONFIG_HOME, "plaza", "bridges", "google-calendar")
CONFIG_FILE = os.path.join(DIRECTORY, "config.json")


def _get_config():
    if not os.path.exists(CONFIG_FILE):
        return {}
    with open(CONFIG_FILE, "rt") as f:
        return json.load(f)


def _save_config(config):
    os.makedirs(DIRECTORY, exist_ok=True)
    with open(CONFIG_FILE, "wt") as f:
        return json.dump(config, f)


def get_client_credentials_path():
    return os.getenv(CLIENT_CREDENTIALS_PATH_ENV, "client_secrets.json")


def get_bridge_endpoint():
    env_val = os.getenv(BRIDGE_ENDPOINT_ENV, None)
    if env_val is not None:
        return env_val

    config = _get_config()
    if config.get(BRIDGE_ENDPOINT_INDEX, None) is None:
        config[BRIDGE_ENDPOINT_INDEX] = input("Programaker bridge endpoint: ")
        if not config[BRIDGE_ENDPOINT_INDEX]:
            raise Exception("No bridge endpoint introduced")
        _save_config(config)
    return config[BRIDGE_ENDPOINT_INDEX]


def get_auth_token():
    env_val = os.getenv(AUTH_TOKEN_ENV, None)
    if env_val is not None:
        return env_val

    config = _get_config()
    if config.get(AUTH_TOKEN_INDEX, None) is None:
        config[AUTH_TOKEN_INDEX] = input("Programaker authentication TOKEN: ")
        if not config[AUTH_TOKEN_INDEX]:
            raise Exception("No authentication token introduced")
        _save_config(config)
    return config[AUTH_TOKEN_INDEX]
