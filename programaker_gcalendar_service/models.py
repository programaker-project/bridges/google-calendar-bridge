from sqlalchemy import (Column, ForeignKey, Integer, MetaData, String, Table,
                        Text, UniqueConstraint)

metadata = MetaData()

GcalendarUsers = Table(
    "GCALENDAR_USER_REGISTRATION",
    metadata,
    Column("id", Integer, primary_key=True, autoincrement=True),
    Column("gcalendar_credentials", Text()),
)

PlazaUsers = Table(
    "PLAZA_USERS",
    metadata,
    Column("id", Integer, primary_key=True, autoincrement=True),
    Column("plaza_user_id", String(36), unique=True),
)

PlazaUsersInGcalendar = Table(
    "PLAZA_USERS_IN_GCALENDAR",
    metadata,
    Column("plaza_id", Integer, ForeignKey("PLAZA_USERS.id"), primary_key=True),
    Column(
        "gcalendar_id",
        Integer,
        ForeignKey("GCALENDAR_USER_REGISTRATION.id"),
        primary_key=True,
    ),
    __table_args__=(UniqueConstraint("plaza_id", "gcalendar_id")),
)
