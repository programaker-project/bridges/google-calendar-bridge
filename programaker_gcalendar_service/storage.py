import os
import re

import sqlalchemy
from xdg import XDG_DATA_HOME

from . import models

DB_PATH_ENV = "GCALENDAR_BRIDGE_DB_PATH"

if os.getenv(DB_PATH_ENV, None) is None:
    # Support old environment variable
    DB_PATH_ENV = "PLAZA_GCALENDAR_BRIDGE_DB_PATH"


if os.getenv(DB_PATH_ENV, None) is None:
    _DATA_DIRECTORY = os.path.join(XDG_DATA_HOME, "plaza", "bridges", "google-calendar")
    CONNECTION_STRING = "sqlite:///{}".format(
        os.path.join(_DATA_DIRECTORY, "db.sqlite3")
    )
else:
    CONNECTION_STRING = os.getenv(DB_PATH_ENV)


class EngineContext:
    def __init__(self, engine):
        self.engine = engine
        self.connection = None

    def __enter__(self):
        self.connection = self.engine.connect()
        return self.connection

    def __exit__(self, exc_type, exc_value, tb):
        self.connection.close()


class StorageEngine:
    def __init__(self, engine):
        self.engine = engine

    def _connect_db(self):
        return EngineContext(self.engine)

    def _add_gcalendar_credentials(self, conn, credentials):
        insert = models.GcalendarUsers.insert().values(
            gcalendar_credentials=credentials
        )
        result = conn.execute(insert)
        return result.inserted_primary_key[0]

    def _get_or_add_programaker_user(self, conn, programaker_user):
        check = conn.execute(
            sqlalchemy.select([models.PlazaUsers.c.id]).where(
                models.PlazaUsers.c.plaza_user_id == programaker_user
            )
        ).fetchone()

        if check is not None:
            return check.id

        insert = models.PlazaUsers.insert().values(plaza_user_id=programaker_user)
        result = conn.execute(insert)
        return result.inserted_primary_key[0]

    def register_user(self, programaker_user, credentials):
        with self._connect_db() as conn:
            gcalendar_id = self._add_gcalendar_credentials(conn, credentials)
            programaker_id = self._get_or_add_programaker_user(conn, programaker_user)
            insert = models.PlazaUsersInGcalendar.insert().values(
                plaza_id=programaker_id, gcalendar_id=gcalendar_id
            )
            conn.execute(insert)

    def get_gcalendar_credentials_from_programaker_id(self, programaker_user):
        with self._connect_db() as conn:
            programaker_id = self._get_or_add_programaker_user(conn, programaker_user)

            join = sqlalchemy.join(
                models.GcalendarUsers,
                models.PlazaUsersInGcalendar,
                models.GcalendarUsers.c.id
                == models.PlazaUsersInGcalendar.c.gcalendar_id,
            )
            results = conn.execute(
                sqlalchemy.select(
                    [
                        models.GcalendarUsers.c.gcalendar_credentials,
                    ]
                )
                .select_from(join)
                .where(models.PlazaUsersInGcalendar.c.plaza_id == programaker_id)
            ).fetchall()
            return [dict(zip(["credentials"], row)) for row in results]


def get_engine():
    # Create path to SQLite file, if its needed.
    if CONNECTION_STRING.startswith("sqlite"):
        db_file = re.sub("sqlite.*:///", "", CONNECTION_STRING)
        os.makedirs(os.path.dirname(db_file), exist_ok=True)

    engine = sqlalchemy.create_engine(CONNECTION_STRING)
    metadata = models.metadata
    metadata.create_all(engine)

    return StorageEngine(engine)
